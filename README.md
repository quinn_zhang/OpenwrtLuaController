#毕业设计论文

绪论（简介）

。wifi网络介绍

无线设备的种类。路由的知识介绍（抄书）

。Netduino介绍

。现有模块分析

嵌入式linux介绍

openwrt介绍

。openwrt 开发环境搭建

。编译ser2net



httpd服务器介绍

模块功能及其实现

。1. http服务器
。2. 网络数据转串口
。3. 修改模块设置     -----lua编程语言介绍
+4. 虚拟AP模式
+5. 基础网模式
。6. 网页配置端口

netduino与wifi模块应用

led小灯实验

总结及展望




##一、绪论

##二、无线网络（概述）

所谓无线网络,是指不需要布线即可实现计算机互联的网络。无线网络的适用范围非常广泛，可以说，凡是可以通过布线而建立的网络环境，无线网络也同样能够搭建，而通过传统布线无法解决的环境或者行业，却正是无线网络大显身手的地方。

无线局域网与传统以太网最大的区别就是对周围环境没有特殊要求，总而言之一句话，只要电磁波能辐射到的地方就可以搭建无线局域网，因此也就产生了多种多样的无线局域网组建方案。但是在实施过程中应根据实际需求和硬件条件选择一种性价比最高的设计方案，以免造成不必要的浪费。

####特点：

无线局域网利用电磁波在空气中发送和接受数据，而不需要线缆介质。无线局域网的数据传输速率现在已经能够达到108Mbps，甚至300Mbps，并可使传输距离达到20km以上。无线网络是对有线网络的一种补充和扩展，使网上的计算机具有可移动性，能快速方便的地解决使用有线方式不易实现的网络联通问题。

与有线网络相比，无线局域网有以下优点。

- 安装便捷。一般在网络建设中，施工周期最长、对周边影响最大的，就是网络布线施工工程。对于有线网络在施工过程中，往往要破墙掘地、穿线架管，而无线局域网最大的优势就是免去或者减少了网络布线的工作量，一般只要安装一个或者多个接入点AP设备，就可以建立覆盖整个建筑或者地区的局域网络。

-  使用灵活。由于有线网络却收啊灵活性，要求在网络规划是尽可能地考虑未来发展的需要，这就往往导致预设大量利用率较低的信息点。而一旦网络的发展超出了设计规划，又要花费较多费用进行网络改造，而无线局域网可以避免或减少以上情况的发生。对于无线局域网而言，只要在无线网的信号覆盖区域内，任何一个位置都可以接入网络。

- 易于扩展。无线局域网有多种配置方式，能够根据需要灵活选择。这样，无线局域网就能胜任从只有几个用户的小型局域网到有上千用户的大型网络，并且能够提供像“漫游”等有线网络无法提供的特性。

由于无线局域网具有多方面的优点，所以发展十分迅速。最近几年，无线局域网已经在医院、商店、工厂、和学校等不适合网络布线的场合得到了广泛的应用。

但与有线网络相比，无线网络也具有一定的局限性，也正是因为如此，目前无线网络只能作为有线网络的补充，而不能完全替代有线网络。具体内容如下。

- 设备价格昂贵。相对而言，无线网络设备（尤其是专业级设备）的价格往往较高。当然这里所谓的昂贵，知识相对于有线网络产品而言价格要昂贵。无线设备费用投入的增加，无形中增加了组建网络总成本的投入。

- 覆盖范围小。一个无线接入点的覆盖半径往往只有几米或者几十米，安装有专用无线天线的无线网络则可以达到几百米，然而，借助于传统的以太网络，将多个无线接入点连接在一起，仍然可以实现覆盖整个部门和单位的目标。

- 网络速度慢。与当前桌面接入达到100Mbps~1000Mbps相比，无线网络的54Mbps~300Mbps，要显得慢得多。同时，无线网络的带宽是共享机制，也就是说，是有若干无线接入用户共享这个连接带宽。当然，这样的传输速率用于普通办公和日常用用也绰绰有余了。

####无线集线设备

与有线网络相同，在无线网络中同样需要集线设备，但与有线网络不同的是，在无线网络中使用到的集线设备主要是无线路由器和无线AP。其中无线路由器主要应用于小型无线网络，而无线AP则可以应用于大中型无线网络中。

#####无线路由器

事实上无线路由器是无线AP和宽带路由器的结合，借助于无线路由器，可以实现家庭或小型网络的无线互联和Internet共享。

#####无线接入点

无线接入点或称无线AP（Access Point），其作用类似于以太网中的集线设备，用于无线终端设备（如便携式计算机、无线打印机、无线摄像头等）提供无限网络接入。通常情况下，一个AP最多可以支持多达30台计算机的接入，但为了保证无线AP的性能，建议数量以不要超过20台为宜.

#####无线网桥

用于点对点或者多点连接时，应当使用无线网桥。由于无线网桥往往用于实现网络之间的互联，并且检修和维护非常困难，因此，对产品性能、传输速率和稳定性都要求较高。

为了便于实现对网络设备的统一部署诶好管理，无线网桥建议选择与其他网络设备（特别是交换机等）同一厂商的产品。当然，互相连接的无线网桥更是必须选用同一厂商，同一品牌的产品。

####无线网络标准

虽然无线网络使用到传输介质是不可见电磁波，但仍需要像有线网络一样，在通信的无线设备的两端使用相同的协议标准。随着技术的发展，无线网络协议标准也在不断的发展和更新中，从某种意义上来说，技术越先进无线网络的传输速度越快，例如802.11g速度可以达到54Mbps，而802.11n的速度则可以达到108Mbps。

IEEE 802.11标准是IEEE指定的无线局域网标准，主要是对网络的物理层和媒质访问控制层进行了规定。目前，已经产品化的无线网络标准主要有4种，即802.11b、802.11g、802.11a和802.11n。

下面详细介绍一下IEEE 802.11n标准：

IEEE 802.11n标准要求802.11n产品能够在包含802.11g和802.11b的混合模式下运行，且具有向下兼容性，在一个802.11n无线网络中，接入用户包括802.11b、802.11g和802.11n的用户，而且所有用户都采用自己的标准同时向无线接入点进行通信。也就是说，在连接过程中，所有类型的传输可以实现共存，从而能够更好地保障用户的投资。由此可见，IEEE 802.11n拥有比IEEEE 802.11g更高的兼容性。

IEEE 802.11n 标准具有以下特点：

1. 传输速率成倍提升

802\.11n 可以将WLAN的传输速率提高至108Mbps，甚至高达600Mbps，即在理想状况下，802.11N提供的传输速率要比802.11g高10倍左右

2. 覆盖范围大大增加

802\.11n 采用智能天线技术，通过多组独立天线组成的天线阵列系统，动态地调整波束的方向，802.11n保证让用户接收到稳定的信号，并减少其他噪声信号的干扰，覆盖范围可扩大到几平方千米。

3. 全面兼容各标准

802\.11n 通过软件无线电技术,解决了不同标准采用不同的工作频段、不同的调制方式，造成系统间难以互通，移动性差的问题。
 


####嵌入式linux及Openwrt介绍


#####嵌入式系统概述


英国的电气工程师学会(The Institutions of Electrical Engineers，IEE)给“嵌入式系统"下定义：嵌入式系统是控制、监视或者辅助设备、机器和车间运行的装置。不严格的说，它是任意包含一个或数个可编程计算机的设备，但是这个设备不是作为通用计算机而设计的。嵌入式系统是一个计算机硬件和软件的集合体，它以应用为中心，以计算机技术为基础，并对可靠性、成本、体积、功耗严格要求。嵌入式系统主要具有四项特性：执行特定的操作，完成特定的功能；以微处理器为核心，辅以一些必要的外设；有严格的稳定性要求，某些系统还要求有严格的实时性；一切操作由微处理器完成，不需要人的干预。

嵌入式系统一般指非PC系统，包括硬件及软件两部分。其中硬件包括处理器／微处理器、存储器及外设器件和I／O端口，图形控制器等。嵌入式软件部分包括操作系统软件和应用程序编程。即嵌入式系统是软硬兼施，互惠互利，融为一体，成为产品。

嵌入式系统事实上也算是计算机体系结构中的一个分支，而一个标准的计算机体系中必然包括中央处理单元、内存、输入／输出设备和其他的一些必要外设，因此，嵌入式系统中也具有这些单元，只不过都以比较特殊的形式存在，例如手机的输入设备就是它的特殊键盘，输出设备是它的液晶显示屏。本文所研究的嵌入式wifi模块就是一种嵌入式系统。


#####嵌入式Liunx介绍




嵌入式 Linux是以Linux为基础的嵌入式作业系统，它被广泛应用在移动电话、个人数字助理(PDA)、媒体播放器、消费性电子产品以及航空航天等领域中。

######简介

嵌入式linux 是将日益流行的Linux操作系统进行裁剪修改，使之能在嵌入式计算机系统上运行的一种操作系统。嵌入式linux既继承了Internet上无限的开放源代码资源，又具有嵌入式操作系统的特性。嵌入式Linux的特点是版权费免费;购买费用媒介成本技术支持全世界的自由软件开发者提供支持网络特性免费，而且性能优异，软件移植容易，代码开放，有许多应用软件支持，应用产品开发周期短，新产品上市迅速，因为有许多公开的代码可以参考和移植，实时性能RT_Linux Hardhat Linux 等嵌入式Linux支持，实时性能稳定性好安全性好。

如果分别让10位工程师给出嵌入式系统的定义，将得到10个不同的答案。一般来说，大部分的嵌入式系统执行特定的任务。我们假定最简单的嵌入式系统包括输入/输出功能，以及一些控制逻辑，该系统基于它的配置执行某些类型的功能。按照这个标准，可以认为一个包含实现控制逻辑74123计数器以及一个状态是一个嵌入式系统。也许可以补充说，该系统必须可通过存储在固件中的软件进行编程。这个新的嵌入式系统定义包括输入/输出(I/O)，以及存储在系统固件中的控制逻辑。一个带有鼠标、键盘、网络连接并运行图形用户界面(GUI，graphical user interface)多任务操作系统的桌面计算机显然满足这些要求，但我们能认为它是一个嵌入式系统吗？

如果桌面计算机不是一个嵌入式系统，那么手持设备呢？它们有I/O功能，可以运行存储在固件中的控制逻辑。有人说，桌面计算机和手持设备都有通用计算机设备，可以运行软件来执行许多不同的任务，与之不同的是，嵌入式系统(例如，洗碗机控制器或飞行导航系统)主要是为特定任务而设计的。这种特定的功能限定使嵌入式设备有功能上的唯一性。如果是这样，为什么一些嵌入式系统设计成具有附加的功能，如存储在非易失性存储器中的程序，并且具有运行可以完成原始设计范围之外的任务的多任务操作系统的能力呢？

在过去，区分嵌入式系统和通用计算机比简单得多。例如，可以很容易地区分出一个基于8051的T1分幅卡嵌入式系统和一台Sun UNIX工作站。从功能方面很难区分一台Sun工作站和一个包含PowerPC以及32MB内存和16MB闪存的机顶盒。这样的机顶盒可以运行带GUI的多任务操作系统，可现场升级，可以同时运行多个程序(如视频控制器、数字录像和Java虚拟机)，还可以进行安全的因特网在线交易。很难判断这种机顶盒是否是一个嵌入式系统。显然，硬件性能的提升和价格的下降使通用计算机和嵌入式系统之间的界限变得很模糊，技术的进步使得我们很难定义什么是嵌入式。


######发展历程

嵌入式系统出现于20世纪60年代晚期，它最初被用于控制机电电话交换机，如今已被广泛的应用于工业制造、过程控制、通讯、仪器、仪表、汽车、船舶、航空、航天、军事装备、消费类产品等众多领域。计算机系统核心CPU，每年在全球范围内的产量大概在二十亿颗左右，其中超过80%应用于各类专用性很强的嵌入式系统。一般的说，凡是带有微处理器的专用软硬件系统都可以称为嵌入式系统。

######应用特点

嵌入式Linux的应用领域非常广泛，主要的应用领域有信息家电、PDA 、机顶盒、Digital Telephone、Answering Machine、Screen Phone 、数据网络、Ethernet Switches、Router、Bridge、Hub、Remote access servers、ATM、Frame relay 、远程通信、医疗电子、交通运输计算机外设、工业控制、航空航天领域等。
就是利用Linux其自身的许多特点，把它应用到嵌入式系统里。

Linux做嵌入式的优势，首先，Linux是开放源代码的，不存在黑箱技术，遍布全球的众多Linux爱好者又是Linux开发者的强大技术支持；其次，Linux的内核小、效率高，内核的更新速度很快,linux是可以定制的，其系统内核最小只有约134KB。第三，Linux是免费的OS，在价格上极具竞争力。 Linux还有着嵌入式操作系统所需要的很多特色，突出的就是Linux适应于多种CPU和多种硬件平台，是一个跨平台的系统。到目前为止，它可以支持二三十种CPU。而且性能稳定，裁剪性很好，开发和使用都很容易。很多CPU包括家电业芯片，都开始做Linux的平台移植工作。移植的速度远远超过Java的开发环境。也就是说，如果今天用Linux环境开发产品，那么将来换CPU就不会遇到困扰。同时，Linux内核的结构在网络方面是非常完整的，Linux对网络中最常用的TCP/IP协议有最完备的支持。提供了包括十兆、百兆、千兆的以太网络，以及无线网络，Toker ring(令牌环网)、光纤甚至卫星的支持。所以Linux很适于做信息家电的开发。

还有使用Linux为的是来开发无线连接产品的开发者越来越多。Linux在快速增长的无线连接应用主场中有一个非常重要的优势，就是有足够快的开发速度。这是因为LInux有很多工具，并且Linux为众多程序员所熟悉。因此，我们要在嵌入式系统中使用Linux操作系统。

Linux的大小适合嵌入式操作系统——Linux固有的模块性，适应性和可配置性，使得这很容易做到。另外，Linux源码的实用性和成千上万的程序员热切期望它用于无数的嵌入式应用软件中，导致很多嵌入式Linux的出现，包括：Embedix，ETLinux，LEM，Linux Router Project，LOAF，uCLinux，muLinux，ThinLinux，FirePlug，Linux和PizzaBox Linux

相比微软，Linux的图形界面发展很快，像GNOME,KDE,UTITY等都是很优秀的桌面管理器，并且其背后有着众多的社团支持，可定制性强，已经在Unix和Linux世界普及开来。

######发展前景

有巨大的市场前景和商业机会，出现了大量的专业公司和产品，如Montavista Lineo Emi等，有行业协会如Embedded Linux Consortum等，得到世界著名计算机公司和OEM板级厂商的支持，例如IBM Motorola Intel等。传统的嵌入式系统厂商也采用了Linux策略，如Lynxworks Windriver QNX等，还有Internet上的大量嵌入式Linux爱好者的支持。嵌入式Linux支持几乎所有的嵌入式CPU和被移植到几乎所有的嵌入式OEM板。




#####openwrt介绍

OpenWrt是一个用于嵌入式设备的GNU/Linux发行版，具有强大的扩展性。不同于其他许多用于路由器的发行版，OpenWrt是一个从零开始编写的、功能齐全的、容易修改的路由器操作系统。实际上，这意味着您能够使用您想要的功能而不加进其他的累赘，而支持这些功能工作的linux kernel又远比绝大多数发行版来得新。

OpenWrt是什么？
OpenWrt提供了一个完全可写文件系统及软件包管理，而不是试图建立一个单一的，静态的固件。这将把您从路由器制造商选定的程序功能、提供的配置的桎梏中解放出来，并允许您使用包来定制嵌入式装置，使之适用于任何应用。对于开发人员，OpenWrt提供了一个框架；您可以直接用这框架来建立应用程序，而无需创建一个完整的固件镜像并为它专门定制一个发行版。对于用户来说，这意味着完全定制的自由，这使得您能用卖方难以预料的方式使用嵌入式设备。

开发原因

当Linksys释放 WRT54G/GS 的源码后，网上出现了很多不同版本的 Firmware 去增强原有的功能。大多数的 Firmware 都是99%使用 Linksys的源码，只有1%是加上去的，每一种 Firmware 都是针对特定的市场而设计，这样做有2个缺点，第一个是难以集合各版本Firmware的长处，第二个是这版本距离 Linux 正式发行版越来越远。
OpenWrt 选择了另一条路，它从零开始，一点一点的把各软件加入去，使其接近 Linksys 版 Firmware的功能，而OpenWrt 的成功之处是它的文件系统是可写的，开发者无需在每一次修改后重新编译，令它更像一个小型的 Linux 电脑系统。

OpenWrt 项目由 2004 年 1 月开始, 第一个版本是基于 Linksys 提供的 GPL 源码及 uclibc 中的 buildroot 项目, 这个版本称为 “stable” 版, 在网上至今仍有很多项目使用这个版本, 较为有名 Freifunk-Firmware 和 Sip@Home.
到了2005年初, 一些新的开发人员加入了这项目, 几个月后他们释出了第一个 “experimental” 版本, 这和以前版本不同的是, 这版本差不多完全舍弃了 Linksys 的 GPL 源码, 使用了 buildroot2 作为核心技术, 将 OpenWrt 完全模块化，OpenWrt 使用 Linux 正式发行的核心源码（2.4.30），加上了一些补丁和网络驱动，开发队伍更为OpenWrt添加了许多免费的工具，可以直接把Image写入 Flash (mtd)里面，设定无线功能和VLAN交换功能，这个版本名为“White Russian”，而1.0版本于2005年底公布。

特点

如果对 Linux 系统有一定的认识, 并想学习或接触嵌入式 Linux 的话, OpenWRT很适合。 而且OpenWRT支持各种处理器架构，无论是对ARM，X86，PowerPC或者MIPS都有很好的支持。 其多达3000多种软件包，囊括从工具链(toolchain)，到内核(linux kernel)，到软件包(packages)，再到根文件系统(rootfs)整个体系，使得用户只需简单的一个make命令即可方便快速地定制一个具有特定功能的嵌入式系统来制作固件。

一般嵌入式 Linux 的开发过程, 无论是 ARM, PowerPC 或 MIPS 的处理器, 都必需经过以下的开发过程：

1、 创建 Linux 交叉编译环境；


2、建立 Bootloader；

3、移植 Linux 内核；

4、建立 Rootfs (根文件系统)；

5、安装驱动程序；

6、安装软件；

熟悉这些嵚入式 Linux 的基本开发流程后，不再局限于 MIPS 处理器和无线路由器, 可以尝试在其它处理器, 或者非无线路由器的系统移植嵌入式 Linux, 定制合适自己的应用软件, 并建立一个完整的嵌入式产品。

劣势

    由于CPU内核体系不同，造成很多应用程序移植到OpenWrt上的时候经常崩溃。

    由于ADSL硬件模块的驱动程序没有开放源代码，造成很多ADSL一体无线路由的ADSL模块不能工作而造成功能缺失(RG100A和DB120除外)。

    由于OpenWRT并不是官方发布的路由器固件，所以要使用(刷入该固件)有困难，而且其基于Linux，导致OpenWRT的入门门槛较高。

历史版本

White Russian

OpenWRT的初始版本，从2005年7月的White Russian RC1开始发展，一直到2007年1月才发布White Russian 0.9。

Kamikaze

OpenWRT的第二个版本，从2007年6月开始发布Kamikaze 7.06，一直更新到2010年1月的Kamikaze 8.09.2结束。这期间OpenWRT进行了大量改进，并为它的发展打下了坚实的基础。

Backfire

2010年03月04日，OpenWrt Backfire 10.03 Beta发布，该版本是开源的路由器固件,基于linux,功能强大,支持很多主流的平台和路由器,甚至支持国内的君正jz4740平台,是学习和开发嵌入式,也是工业、 商业应用理想平台。更新方面： brcm-2.4 更新到 2.4.37 kernel ，other targets 更新到 2.6.30 or 2.6.32 ，arm/mips平台的 gcc 更新到 4.3.3 ,powerpc的gcc更新到 4.4.3 ，uClibc 更新到 0.9.30.1 ， Broadcom 11g 芯片组的 b43 无线网卡驱动更新到 2.6 kernel ，支持Atheros 11n ath9k ，支持很多新的ar71xx设备，magicbox归入ppc40x平台 
。
2010年03月25日，OpenWrt 发布 Backfire 10.03-rc1，支持国内留下的 tp-link最新多款11n的路由器,是不是想让路由器支持万能中继呢,或者增加路由器的多wan口支持呢,更多的这些高端路由才有的功能,或许能 让家用路由轻松具备。

2010年04月07日，OpenWRT放出Backfire 10.03正式版。

2010年08月29日OpenWRT放出了Backfire 10.03.1。修正了很多BUG，提高了兼容性。TP-LINK WR841N v2 已经可以正常使用了，包括无线部分和上网部分。 自rc1之后的变动：改进了防DNS重绑定攻击，改进了uhttpd稳定性， Rootfs生成修复(Orion景象)，修正了基于BRCM47XX的PCI初始化，添加了rtl8366 vlan 改变的 整合脚本，还原了一般x86镜像 GRUB控制台，提高了ar71xx系列以太网驱动程序性能，添加了ar7240 交换机驱动，一些swconfig 和 交换机驱动的改进，RDC 使用启动加载器支持波特率，允许原生HID 支持 通过添加 kmod-input-hid，6in4:适当的处理了PPPoE连接并且修复了终端隧道更新。

Attitude Adjustment

于2013年4月25日放出，是现在最稳定的版本。




它能做什么？
OpenWrt并未打算发布现成的可以直接加载到你的嵌入式设备的固件。相反，该框架允许您创建适合自己的特殊需要的一个固件。

虽然有几个项目提供用户界面，以支付共同使用的情况下，OpenWrt是不是最终用户固件。更高级的任务，需要命令行操作和对操作基于Linux系统的基本知识。







WIFI是IEEE定义的无线网技术，在1999年IEEE官方定义802.11标准的时候，IEEE选择并认定了CSIRO发明的无线网技术是世界上最好的无线网技术，因此CSIRO的无线网技术标准，就成为了2010年WIFI的核心技术标准。

Netduino开源平台现在由三款产品组成，Netduino 2，Netduino Plus 2，Netduino GO，以Netduino Plus 2 为例，它的主控制器为意法半导体公司的STM32F405RG，基于ARM公司的Cortex-M4架构，主时钟频率为168MHz，拥有384KB的代码存储空间以及100KB的运行内存，支持TF卡，拥有22个GPIO口，满足物联网节点的基本需求。

目前Netduino平台上拥有一个10mbps的ethernet接口，虽然能够与互联网相连，但是其方便性远远不如无线网络。


##三、netduino介绍

Netduino是一种开源硬件平台，极客和程序员能够使用Netduino创造出更多有趣的电子应用。相比Arduino、和树莓派等产品，Netduino更具备易用性。

Netduino应用程序使用.NETMicro Framework。此编程框架从微软很容易开始使用；对初学者来说，写C#代码非常简单，就就像写一个javascript代码或者创建一个网页一样的简单，全世界成千上万的程序员可以在现有的知识下去开发硬件， .NET Micro Framework提供了一个非常强大的功能集(如事件线程，和逐行调试)。

使用Netduino可以轻易的构建一个传感器和控制器网络，并加以控制，但是现有的Netduino集群控制方案多数为有线网络，布线比较麻烦。而现有的WIFI模块都比较功能单一，难以在AP和INFRA模式之间切换。且数据传输方式多数为串口传输，速度较慢，一块专门为Netduino适配的WIFI模块变变得十分重要。

##现有模块分析与选择

从3月份收到任务书以来，我就进入了毕业设计的工作之中。首先便开始了芯片和模块选型的工作， 根据任务书上的要求，我需要一块能支持SPI接口和wifi热点功能的芯片。具体需求如下

•	支持802.11无线传输协议

•	支持AP模式和基础网模式连接

•	支持SPI接口

依据三个基本要求我在网络和书籍中进行了一番查找，以下是我所寻找到的几个比较有代表性的芯片及解决方案

WM-BG-MR-03（88W8686芯片）+lwIP

WM-BG-MR-03模块简介

wifi模块WM-BG-MR-03 是一款提供 802.11g/b无线传输协议和蓝牙2.1协议的全功能无线模块
它采用MAVELL公司的 88w8686芯片作为其主控芯片，这款芯片是一款低功耗的无线SoC，支持802.11a/b/g的wifi传输制式，最大波特率为54Mbps（802.11a/g模式）或11Mbps（802.11b模式）

WM-BG-MR-03模块所提供的接口为

•	WIFI：SDIO和SPI

•	蓝牙：SDIO和UART

通过查阅其Datasheet得知这款模块没有内置wifi热点，路由转发，还有TCP/IP协议栈

于是我又在网络上找到了一个开源的TCP/IP协议栈lwIP作为其IP协议栈

lwIP简介

lwip是瑞典计算机科学院(SICS)的Adam Dunkels 开发的一个小型开源的TCP/IP协议栈。

LwIP是Light Weight (轻型)IP协议，有无操作系统的支持都可以运行。LwIP实现的重点是在保持TCP协议主要功能的基础上减少对RAM 的占用，它只需十几KB的RAM和40K左右的ROM就可以运行，这使LwIP协议栈适合在低端的嵌入式系统中使用。 lwIP协议栈主要关注的是怎么样减少内存的使用和代码的大小，这样就可以让lwIP适用于资源有限的小型平台例如嵌入式系统。为了简化处理过程和内存要求，lwIP对API进行了裁减，可以不需要复制一些数据。

TI CC3000模块

德州仪器（TI）公司的TI CC3000 模块是一款完备的无线网络处理器，此处理器简化了互联网连通性的实施。

TI 的SimpleLink™ Wi-Fi 解决方案大大降低了主机微控制器 (MCU) 的软件要求，并因此成为使用任一低成本和低功耗MCU 的嵌入式应用的理想解决方案。

TI CC3000 模块减少了开发时间、降低了制造成本、节省了电路板空间、简化了认证，并且大大降低了对 RF 专业知识的要求。 这个完整的平台解决方案包括软件驱动程序，示例程序，API 指南，用户文档和一个世界级的支持社区。

Qualcomm Atheros AR9331芯片 + OpenWrt

Atheros AR9331简介

Atheros AR9331芯片是一个高度完整且高效的802.11n 2.4GHz wifi芯片，它适用于无线AP和无线路由平台的构建

AR9331包括一个MIPS公司的24K系列处理器，五个IEEE 802.3快速以太网交换口一个USB2.0接口，以及一系列的对于UART，SPI，I2S接口的支持。




##搭建OpenWrt开发环境

###一、获取Openwrt源代码

openwrt经过许多年的发展拥有许多版本。作者采用的是Attitude Adjustment
的开发者版本（trunk）版本。获取方式为svn下载

首先在Windows系统上安装TortoiseSVN，然后使用其SVN checkout命令从`svn://svn.openwrt.org/openwrt/trunk/`上下载Openwrt的源代码。

###二、安装Liunx系统

下载完毕后，我们需要进行编译的工作，因为Openwrt是一个基于Linux的路由管理系统，所以其交叉编译的环境最好为Liunx系统，于是我们首先要搭建一个Linux的环境，我采用的方案为Virtualbox虚拟机+Ubuntu 14.04 LTS。

从Virtualbox官网下载Virtualbox安装程序，按照提示进行安装。
从Ubuntu官网下载Ubuntu 14.04 LTS的光盘镜像文件ubuntu-14.04-desktop-i386.iso。
打开Virtualbox，点击”新建“按钮，在弹出的对话框中，在“名称”选项中输入“VirtualLinux”，类型“选项选择”Linux“，”版本”选项选择“Ubuntu （32bit）”后，之后点击“下一步”并按照默认选项操作即可新建一台新的虚拟机

新建成功之后在Virtualbox左侧列表中右键点选VirtualLinux虚拟机，选择”设置“选项，在弹出来的对话框的左侧列表中选择”存储“，点击后，在右侧选项卡里找到”控制器：IDE“选项，选择后点击右侧第一个光盘形状小图标，在弹出的对话框里找到并选择Ubuntu 14.04 LTS的光盘镜像文件ubuntu-14.04-desktop-i386.iso，最后点击”确定“按钮

返回Virtualbox软件主界面后，选择”VirtualLinux“虚拟机，点击”启动“按钮

之后进入Ubuntu安装界面，按照软件提示安装Ubuntu即可。

###三、编译

安装完毕后，将源代码复制到虚拟机的Linux系统中，键盘按下Ctrl+Alt+T组合键，调出Terminal终端，详细步骤如下：

####1.建立非root用户

首先确保当前用户是非root用户，如果是root用户，可以使用

    add user openwrt
    su openwrt

来建立新的名称为openwrt的用户

之后使用`cd`命令将当前目录定位到openwrt源码的根目录


####2.下载和更新资源

#####下载各种依赖库和软件包

在Ubuntu系统下，输入

    apt-get install subversion build-essential libncurses5-dev zlib1g-dev gawk git ccache gettext libssl-dev xsltproc

#####下载Feeds

Feeds是OpenWrt环境所需要的软件包套件。
最重要的feeds有：

- ‘packages’一些额外的基础路由器特性软件

- ‘LuCI’OpenWrt默认的GUI

- ‘Xwrt’另一种可选的GUI界面

下载Feeds时需要能够连接互联网。
在下载之前可以通过查看’feeds.conf.default’文件，来检查哪些文件需要包含在环境中。

执行以下命令

    ./scripts/feeds update -a
    ./scripts/feeds install -a    

####3.进行配置
编译过程使用的交叉编译，交叉编译生成的SDK以及image等文件的类型取决于开发环境、应用硬件、以及源码版本。所以要对自己的环境进行了解，才能进行正确的配置。

我使用的芯片是Atheros 的AR9331芯片，在openwrt属于ar71xx系列

首先执行

    make defconfig
    make prereq

make会检查当前环境是否符合openwrt编译要求，如果发现不符合的情况，请按照程序给出的提示进行重新下载依赖包和重新设置开发环境

检测完毕后，输入

    make menuconfig
    
调出编译配置菜单

通过文本对话框进行选项配置，最主要的配置项有：

Target system（目标系统类型）

Package selection（软件包选择）

Build system settings  （编译系统设置）

Kernel modules  （内核模块）

[*]表示:这个包裹选中编译，并安装在firmware中；

[M]表示：这个软件包选中编译，但并不安装在firmware中。

在退出Menuconfig的时，会提示是否保存配置。

在此我只对target system进行了选择；勾选了Advanced configuration option和Build the OpenWrt SDK选项。

####4.开始编译

（1）一般情况，使用一个简单的命令：

    make
    
（2）在多核处理器系统上为提高速度，可使用（例如用3核处理器）：

    make –j 3
    
（3）在后台进行编译，使用空闲的I/O资源和CPU性能，可使用（例如使用双核处理器）

    onice -c 3 nice -n 20 make -j 2

（4）编译一个单独的软件包（例如在cups软件包）：

    make package/cups/compile V=99

（5）如果特殊原因需要分析编译报错信息：
    
    make V=99 2>&1 |tee build.log |grep -i error

说明：将编译的所有输出信息保存在build.log中，将error信息打印在屏幕上。

（6）一个复杂指令的应用

        ionice -c 3 nice -n 20 make -j 2 V=99 CONFIG_DEBUG_SECTION_MISMATCH=y 2>&1 \|tee build.log |egrep -i '(warn|error)'

说明：将编译的所有输出信息保存在build.log中，将error和warning信息打印在屏幕上。编译过程使用双核CPU，占用后台资源。

####5. 生成镜像（Image）位置

新生成的镜像会默认放在新建的一个bin目录下。例如这次生成的文件在:

    /bin/ar71xx/
 
其中文件主要有几类：

（1）.bin/.trx 文件: 这些都是在我们所选的target-system的类别之下，针对不同路由器型号、版本编译的路由器固件。这些不同路由器的型号和版本是openwrt预先设置好的，我们不需要更改。至于.bin和.trx的区别，一种说法是，第一次刷路由器的时候，需要用.bin文件，如果需要再升级，则不能再使用.bin文件，而需要用.trx文件。原因是，.bin是将路由器的相关配置信息和.trx封装在一起而生成的封包，也就是说是包含路由器版本信息的.trx。在第一次刷固件的时候，我们需要提供这样的信息，而在后续升级时，则不再需要，用.trx文件即可。

（2）packages文件夹: 里面包含了我们在配置文件里设定的所有编译好的软件包。默认情况下，会有默认选择的软件包。

（3）OpenWrt-SDK.**.tar.bz2:  这个也就是我们定制编译好的OpenWRT SDK环境。我们将用这个来进行OpenWrt软件包的开发。例如，我所编译好的SDK环境包为：/bin/ar71xx/OpenWrt-SDK-ar71xx-for-linux-i686-gcc-4.8-linaro_uClibc-0.9.33.2

可以从名称上看出，target system是ar71xx，host system是Linux-i686，使用的编译工具以及库是4.3.3+cs_uClibc-0.9.30.1。

（4）md5sums 文件: 这个文件记录了所有我们编译好的文件的MD5值，来保证文件的完整性。因为文件的不完整，将很容易使得路由器无法启动。

需要主要的是，编译完成后，一定要将编译好的bin目录进行备份（如果里面东西对你很重要的话），因为在下次编译之前，执行make clean 会将bin目录下的所有文件给清除掉。

至此，Openwrt开发环境搭建完毕。


##模块功能及其实现

模块功能


1. http服务器

为了模块的使用方便，本模块采取网页配置的方式来对模块进行各种设置，而要采取网页的方式来进行配置的话就需要一个HTTP服务器，我采用的是uhttpd服务器。

####uhttpd简介

uhttpd 是一个又Openwrt和Luci开发者开发的web服务器。它的目标是成为一个有效率的和稳定的服务器，适合于在嵌入式设备和Openwrt的设置框架（UCI）使用的轻量级任务的处理。实际使用中，uhttpd已经被默认设置为Luci的网页接口以用来管理Openwrt。另外，它还提供所有现在的web服务器程序能提供的全部功能。

####uhttpd的特性

由于被搭建成为一个通用的http服务器，uhttpd不仅仅只是用来做openwrt的web界面，它还有着许多现代服务器程序同样拥有的功能。它能够支持TLS（SSL），CGI和Lua。它是一个单线程的服务器程序但是支持复数的程序实例（例如：多个监听端口，每一个有自己独立的根目录和其他功能）

对比其他的服务器，uhttpd同时支持在进程中运行Lua程序，这将会提升服务器的脚本执行速度。

####配置uhttpd

uhttpd的配置已经很好的通过UCI整合在了Openwrt的用户界面系统中。关于uhttpd的配置文件的路径为：`/etc/config/uhttpd`。uhttpd的配置完全依赖于这个文件 ***，下面是关于uhttpd配置文件的详细描述：【PS字数不够就写】***



2. 网络数据转串口

模块的重要基本功能为网络数据传输至Netduino单片机，原计划是通过spi串口进行传输，但是经过查找资料，我发现Netduino对于UART串口的支持更为优秀，Netduino有集成在CPU上的独立的UART串口解析设备，而SPI总线协议却需要通过软件模拟实现，大大降低了本来就不丰富的CPU资源的使用率。

####一、串口简介


串行接口（Serial port）又称“串口”，主要用于串行式逐位数据传输。常见的有一般计算机应用的RS-232（使用 25 针或 9 针连接器）和工业计算机应用的半双工RS-485与全双工RS-422。

串行接口按电气标准及协议来分，包括RS-232-C、RS-422、RS485、USB等。 RS-232-C、RS-422与RS-485标准只对接口的电气特性做出规定，不涉及接插件、电缆或协议。USB是近几年发展起来的新型接口标准，主要应用于高速数据传输领域。


**RS-232-C**

也称标准串口，是目前最常用的一种串行通讯接口。它是在1970年由美国电子工业协会（EIA）联合贝尔系统、 调制解调器厂家及计算机终端生产厂家共同制定的用于串行通讯的标准。它的全名是“数据终端设备（DTE）和数据通讯设备（DCE）之间串行二进制数据交换接口技术标准”。传统的RS-232-C接口标准有22根线，采用标准25芯D型插头座。自IBM PC/AT开始使用简化了的9芯D型插座。至今25芯插头座现代应用中已经很少采用。电脑一般有两个串行口：COM1和COM2，9针D形接口通常在计算机后面能看到。现在有很多手机数据线或者物流接收器都采用COM口与计算机相连。


####二、ser2net的编译

#####1.ser2net简介

ser2net是一个存在于sourcesforge的开源项目，其功能是将网络连接转换为串口数据

#####2.下载源码并创建工程

在其sourcesforge的项目主页（网址：http://sourceforge.net/projects/ser2net/） 上下载其最新版本的代码，

将前文编译成功的OpenwrtSDK（文件名：OpenWrt-SDK-ar71xx-for-linux-i686-gcc-4.8-linaro_uClibc-0.9.33.2.tar.gz）

解压至任一目录下，进入终端，使用命令`cd package`打开package目录

用命令`mkdir ser2net`新建一个名为ser2net的目录

进入目录，使用命令`mkdir src`新建一个名为src的目录

使用命令`touch Makefile`新建一个名为Makefile的文件

目录结构如下：

```
/OpenWrt-SDK-ar71xx-for-linux-i686-gcc-4.8-linaro_uClibc-0.9.33.2

|
|--package
        |
        |--ser2net
                |
                |--src(目录)
                |
                |--Makefile
```

使用命令`vim Makefile`进入编辑器，编辑Makefile文件

键入以下代码

```
#
# Copyright (C) 2006-2013 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

include $(TOPDIR)/rules.mk

PKG_NAME:=ser2net
PKG_VERSION:=2.9.1
PKG_RELEASE:=1

PKG_BUILD_DIR := $(BUILD_DIR)/$(PKG_NAME)

PKG_FIXUP:=autoreconf
PKG_INSTALL:=1

include $(INCLUDE_DIR)/package.mk

define Package/ser2net
  SECTION:=net
  CATEGORY:=Network
  TITLE:=Serial port TCP/IP redirector
  URL:=http://sourceforge.net/projects/ser2net/
endef

define Package/ser2net/description
  This is ser2net, a program for allowing network connections to serial ports.
  See the man page for information about using the program. Note that ser2net
  supports RFC 2217 (remote control of serial port parameters), but you must
  have a complient client.
endef

define Package/ser2net/conffiles
/etc/ser2net.conf
endef

# uses GNU configure

define Package/ser2net/install
	$(INSTALL_DIR) $(1)/usr/sbin
	$(INSTALL_BIN) $(PKG_INSTALL_DIR)/usr/sbin/ser2net $(1)/usr/sbin/
	$(INSTALL_DIR) $(1)/etc
	$(INSTALL_CONF) $(PKG_BUILD_DIR)/ser2net.conf $(1)/etc/
endef

$(eval $(call BuildPackage,ser2net))

```

以下是对这个Makefile进行的分析：

######软件包变量

建立一个软件包不需要太多工作；大部分工作都隐藏在其它的 makefiles 中，编写工作被抽象成对几个变量的赋值。

- PKG_NAME -软件包的名字, 在 menuconfig 和 ipkg 显示
- PKG_VERSION -软件包的版本，主干分支的版本正是我们要下载的
- PKG_RELEASE -这个 makefile 的版本
- PKG_BUILD_DIR -编译软件包的目录
- PKG_SOURCE -要下载的软件包的名字，一般是由 PKG_NAME 和 PKG_VERSION 组成
- PKG_SOURCE_URL -下载这个软件包的链接
- PKG_MD5SUM -软件包的 MD5 值
- PKG_CAT -解压软件包的方法 (zcat, bzcat, unzip)
- PKG_BUILD_DEPENDS -需要预先构建的软件包，但只是在构建本软件包时，而不是运行的时候。它的语法和下面的DEPENDS一样。

PKG_*变量定义了从何处下载这个软件包；@SF是表示从sourceforge网站下载的一个特殊关键字。md5sum用来检查从网上下载的软件包是否完好无损。PKG_BUILD_DIR定义了软件包源代码的解压路径。

######BuildPackage宏

上面文件底部的最后一行是最为关键的BuildPackage宏。

BuildPackage宏是在$(INCLUDE_DIR)/package.mk文件里定义的。BuildPackage宏只要求一个参数，即要编译的软件包名，在本例中是"bridge"。所有其他信息都通过宏来获得，这提供了一种内在的简洁性。比如BuildPackage需要软件包的一大串描述信息，我们并不要向它传递冗长的参数，因为我们已经约定描述信息定义在DESCRIPTION宏，BuildPackage从里面读取就可以了。

相关BuildPackage宏如下：

Package/

描述软件包在menuconfig和ipkg中的信息，可以定义如下变量：

- SECTION - 软件包类型 (尚未使用)
- CATEGORY - menuconfig中软件包所属的一级目录，如Network
- SUBMENU - menuconfig中软件包所属的二级目录，如dial-in/up
- TITLE - 软件包标题
- DESCRIPTION - 软件包的详细说明
- URL - 软件的原始位置，一般是软件作者的主页
- MAINTAINER - (optional) 软件包维护人员
- DEPENDS - (optional) 依赖项，运行本软件依赖的其他包

Package/conffiles (可选)

软件包需要复制的配置文件列表，一个文件占一行

Build/Prepare (可选)

一组解包源代码和打补丁的命令，一般不需要。

Build/Configure (可选)

如果源代码编译前需要configure且指定一些参数，就把这些参数放在这儿。否则可以不定义。

Build/Compile (可选)

编译源代码命令。

Package/install

软件安装命令，主要是把相关文件拷贝到指定目录，如配置文件。

Package/preinst

软件安装之前被执行的脚本，别忘了在第一句加上#!/bin/sh。如果脚本执行完毕要取消安装过程，直接让它返回false即可。

Package/postinst

软件安装之后被执行的脚本，别忘了在第一句加上#!/bin/sh。

Package/prerm

软件删除之前被执行的脚本，别忘了在第一句加上#!/bin/sh。如果脚本执行完毕要取消删除过程，直接让它返回false即可。

Package/postrm

软件删除之后被执行的脚本，别忘了在第一句加上#!/bin/sh。


#####3.编译

将当前目录返回到SDK：
执行make进行编译。如果一切顺利，最后的结果会保存在OpenWrt-SDK-ar71xx-for-linux-i686-gcc-4.8-linaro_uClibc-0.9.33.2/bin/ar71xx/packages目录下，名称为ser2net 2.9.1_1_ar71xx.ipk。

#####4.安装

***【PS可以扩展】***

使用WinScp将编译好的程序安装包复制到模块上，通过opkg包管理工具进行安装

执行`opkg install ser2net 2.9.1_1_ar71xx.ipk`命令

#####5.配置

***【PS可以扩展配置文件怎么写】***



(写怎么编译ser2net，makefile)

3.  修改模块配置


本模块采用openwrt内置的UCI系统进行对模块的配置

OpenWrt的所有配置文件皆位于/etc/config/目录下。每个文件大致与它所配置的那部分系统相关。可用文本编辑器、"uci" 命令行实用程序或各种编程API(比如 Shell, Lua and C)来编辑/修改这些配置文件。

文件位置	描述
基本配置
/etc/config/dhcp	dnsmasq和DHCP的配置
/etc/config/dropbear	SSH服务端选项
/etc/config/firewall	中央防火墙配置
/etc/config/network	交换，接口和路由配置
/etc/config/system	杂项与系统配置
/etc/config/timeserver	rdate的时间服务器列表
/etc/config/wireless	无线设置和无线网络的定义
IPv6
/etc/config/ahcpd	Ad-Hoc配置协议(AHCP) 服务端配置以及转发器配置
/etc/config/aiccu	AICCU 客户端配置
/etc/config/dhcp6c	WIDE-DHCPv6 客户端配置
/etc/config/dhcp6s	WIDE-DHCPv6 服务端配置
/etc/config/gw6c	GW6c 客户端配置
/etc/config/radvd	路由通告 (radvd) 配置
其他
/etc/config/etherwake	以太网唤醒: etherwake
/etc/config/fstab	挂载点及swap
/etc/config/hd-idle	另一个可选的硬盘空闲休眠进程(需要路由器支持usb硬盘)
/etc/config/httpd	网页服务器配置选项(Busybox 自带httpd, 已被舍弃)
/etc/config/luci	基础 LuCI 配置
/etc/config/luci_statistics	包统计配置
/etc/config/mini_snmpd	mini_snmpd 配置
/etc/config/mountd	OpenWrt 自动挂载进程(类似autofs)
/etc/config/multiwan	简单多WAN出口配置
/etc/config/ntpclient	ntp客户端配置，用以获取正确时间
/etc/config/pure-ftpd	Pure-FTPd 服务端配置
/etc/config/qos	QoS配置(流量限制与整形)
/etc/config/samba	samba配置(Microsoft文件共享)
/etc/config/snmpd	SNMPd(snmp服务进程) 配置
/etc/config/sshtunnel	sshtunnel配置
/etc/config/stund	STUN 服务端配置
/etc/config/transmission	BitTorrent配置
/etc/config/uhttpd	Web服务器配置(uHTTPd)
/etc/config/upnpd	miniupnpd UPnP服务器配置
/etc/config/ushare	uShare UPnP 服务器配置
/etc/config/vblade	vblade 用户空间AOE(ATA over Ethernet)配置
/etc/config/vnstat	vnstat 下载器配置
/etc/config/wifitoogle	使用按钮来开关WiFi的脚本
/etc/config/wol	Wake-on-Lan: wol
/etc/config/znc	ZNC 配置


####配置文件语法

文件语法
在UCI的配置文件通常包含一个或多个配置语句，包含一个或多个用来定义实际值的选项语句的所谓的节。

下面是一个简单的配置示例文件：

package 'example'

config 'example' 'test'
        option   'string'      'some value'
        option   'boolean'     '1'
        list     'collection'  'first item'
        list     'collection'  'second item'
config 'example' 'test' 语句标志着一个节的开始。这里的配置类型是example，配置名是test。配置中也允许出现匿名节，即自定义了配置类型，而没有配置名的节。配置类型对应配置处理程序来说是十分重要的，因为配置程序需要根据这些信息来处理这些配置项。
option 'string' 'some value'  和  option 'boolean' '1'  定义了一些简单值。文本选项和布尔选项在语法上并没有差异。布尔选项中可以用'0' ， 'no'， 'off'， 或者'false'来表示false值，或者也可以用'1'， 'yes'，'on'或者'true'来表示真值。
以list关键字开头的多个行，可用于定义包含多个值的选项。所有共享一个名称的list语句，会组装形成一个值列表，列表中每个值出现的顺序，和它在配置文件中的顺序相同。如上例种中，列表的名称是'collection'，它包含了两个值，即'first item'和'second item'。
'option'和'list'语句的缩进可以增加配置文件的可读性，但是在语法不是必须的。
通常不需要为标识符和值加引号，只有当值包括空格或者制表符的时候，才必须加引号。同时，在使用引号的时候，可以用双引号代替单引号。

下面列举的例子都是符合uci语法的正确配置：

option example value
option 'example' value
option example "value"
option "example"    'value' 
option   'example' "value"
反之，以下配置则存在语法错误

option 'example" "value'  (引号不匹配)
option example some value with space  (值中包含空格，需要为值加引号)
还有一点是必须知道的，即UCI标识符和配置文件名称所包含的字符必须是由a-z， 0-9和_组成。 选项值则可以包含任意字符，只要这个值是加了引号的。

####UCI简介

"uci"是"Unified Configuration Interface"(统一配置界面)的缩写,意在OpenWrt整个系统的配置集中化。

系统配置应容易，更直接且在此有文档描述，从而使你的生活更轻松！

（它是White Russian系列OpenWrt基于nvram的配置的后继改进。）

许多程序在系统某处拥有自己的配置文件，

比如/etc/network/interfaces, /etc/exports,  /etc/dnsmasq.conf或者 /etc/samba/samba.conf，

有时它们还使用稍有不同的语法。

在OpenWrt中你无需为此烦恼，我们只需更改UCI配置文件！

不需要为了某个更改起效而重启系统。

####查找设置并修改实例

以修改wifi模块的ssid名称为例：

首先使用

    uci export 
    
命令，显示结果如下：

```
package wireless

config wifi-device 'radio0'
    option type 'mac80211'
    option hwmode '11ng'
    option path 'platform/ar933x_wmac'
    list ht_capab 'SHORT-GI-20'
    list ht_capab 'SHORT-GI-40'
    list ht_capab 'RX-STBC1'
    list ht_capab 'DSSS_CCK-40'
    option disabled '0'
    option channel 'auto'
    option htmode 'HT40+'
    option country 'CN'
    option noscan '1'

config wifi-iface
    option device 'radio0'
    option network 'lan'
    option mode 'ap'
    option encryption 'none'
    option ssid 'OPENWRT'
```

可以看到在“wireless”配置文件下有可以修改SSID的选项

于是使用“uci show”命令输出具体的配置信息

命令显示如下：

```
wireless.radio0=wifi-device
wireless.radio0.type=mac80211
wireless.radio0.hwmode=11ng
wireless.radio0.path=platform/ar933x_wmac
wireless.radio0.ht_capab=SHORT-GI-20 SHORT-GI-40 RX-STBC1 DSSS_CCK-40
wireless.radio0.disabled=0
wireless.radio0.channel=auto
wireless.radio0.htmode=HT40+
wireless.radio0.country=CN
wireless.radio0.noscan=1
wireless.@wifi-iface[0]=wifi-iface
wireless.@wifi-iface[0].device=radio0
wireless.@wifi-iface[0].network=lan
wireless.@wifi-iface[0].mode=ap
wireless.@wifi-iface[0].encryption=none
wireless.@wifi-iface[0].ssid=OPENWRT
```

使用“uci set wireless.@wifi-iface[0].ssid=pipicoldnet”命令修改配置文件

再输入“uci commit”命令提交所进行的修改

最后，输入“wifi”命令，重新启动wifi服务

这是再次用其他设备搜索空间中的wifi信号，能看到原来的SSID为“OPENWRT”的AP已经被修改为“pipicoldnet”



（uci命令尝试）

4. 多SSID模式
<http://search.cnki.com.cn/search.aspx?q=%u591Assid>

本模块支持多SSID服务。

####概述

在单台AP上使用多个逻辑AP（VAP），而多个逻辑AP具有不同的SSID和能力集，即多个SSID用户接入点，我们可以设置不同的SSID，支持不同的业务、计费、安全策略。即在单台AP上开启不同的SSID，用户通过关联上不同的SSID，使用不同的业务、计费与安全策略。

在这种方式下，我们可以在WLAN的认证、安全、计费等方面针对不同的SSID不同的用户群体有着不同的选择，业务开展更加灵活，用户针对自身不同的需求也可以有不同的选择。

####配置原理

采用OpenWrt自带的UCI系统进行配置。

主要相关配置文件为：“/etc/config/wireless”

wireless文件构成如下

#####无线设置包含的部分

一个典型的无线设置文件至少应该包含两个方面的内容，即至少一个wifi device 和 至少一个与之相关的wifi interface 配置。

wifi device 的设置指的是一般无线电通讯相关的参数，例如设备硬件（网卡芯片驱动程序类型），信道，频率，发射功率等；

wifi interface 的设置指的是 wifi device 的工作模式，essid，无线加密方式等。

wifi device 和 wifi interface 具有关联性，首先是设置好一个 wifi device 的参数，然后再设置与这个 wifi device 相关的 wifi interface 参数，从而构造出一个可以有效工作的无线局域网环境。

######wifi-device 配置项

wifi-device 所配置的是指设备中无线通讯硬件，很多情况中，一个设备只有一个无线通讯接口，所以只有一个 wifi-device 配置项，如果有多个无线通讯设备则会有多个 wifi-device 配置项，每个配置项用来指定不同的接口。

一个最小的 wifi-device 配置就像下面的例子，需要注意的是其中所指定的不同的芯片类型和驱动。

    config 'wifi-device' 'wl0'
        option 'type'    'broadcom'
        option 'channel' '6'
        
其中：

wl0 是无线网卡的内置标识符

broadcom 表示芯片或驱动程序的类型

6 指定无线网卡工作的无线频道

######wifi interface配置项

每个完整的wireless配置文件都至少拥有一个wifi interface配置项，用以来定义一个硬件，部分的设备支持多个无线接口

一个最小的wifi interface配置项如下：

    config 'wifi-iface'
        option 'device'     'wl0'
        option 'network'    'lan'
        option 'mode'       'ap'
        option 'ssid'       'MyWifiAP'
        option 'encryption' 'psk2'
        option 'key'        'secret passphrase'
        
wl0 是对应的wifi硬件的名称

lan 指出了其在“/etc/config/network”中匹配的网络

ap 是其工作的模式，在这个例子里使用的是接入点（AP）模式

myWifiAP 是这个模块广播的ssid

psk2 是这个无线网络所使用的加密格式

secret passphrase 是这个wifi网络的密码

####实现过程

本模块使用Lua脚本程序进行配置，通过在"/www/cgi-bin/virtual_ap"文件进行控制

以下是此文件的代码分析：

此文件分为两个部分：

1. 请求解析部分

通过使用Lua的系统函数“os.getenv”获取从其他页面传递过来的消息，并用自定义函数“string_split“提取消息内容。

”string_split“介绍

输入参数：

szFullString —— 待分割的字符串

szSeparator —— 进行分割所使用的分隔符

返回值：

返回一个依据分隔符对字符串进行分割所得到的数组





2. 命令执行部分

使用Lua的系统函数“os.execute”执行相对应的UCI命令

所使用的UCI命令如下：

当新建一个SSID时：

```
uci add wireless wifi-iface
uci set wireless.@wifi-iface[1].device=radio0
uci set wireless.@wifi-iface[1].network=lan
uci set wireless.@wifi-iface[1].mode=ap
uci set wireless.@wifi-iface[1].encryption=none
uci set wireless.@wifi-iface[1].ssid=pipi2net
uci commit
```

删除已经建立的SSID：

```
uci delete wireless.@wifi-iface[1]
uci commit

```

5. 基础网模式和AP模式切换

在实际使用过程中，我们不仅仅需要使用AP模式，同时我们也需要使用基础网模式来连入其他AP发出的无线网络，用以充当客户端。

本功能的配置原理与多SSID功能相同

不过需要修改相应的UCI命令如下：

    uci set wireless.@wifi-iface[0].network=wan
    uci set wireless.@wifi-iface[0].mode=sta
    uci set wireless.@wifi-iface[0].ssid=lab523
    uci commit
    


6. 网页配置模块功能

***【PS直接抄2014041401  blog】***

本模块在路由器的地址的80端口开放了一个网页版的配置程序，使得用户能够方便、直观的修改模块的各种配置。

同时Openwrt路由系统自带了一个由Lua编写的快速配置接口：LuCI

对于网页配置模块功能，我采用“Lua”脚本语言进行编写。

####Lua脚本语言简介

Lua 是一个小巧的脚本语言。是巴西里约热内卢天主教大学（Pontifical Catholic University of Rio de Janeiro）里的一个研究小组，由Roberto Ierusalimschy、Waldemar Celes 和 Luiz Henrique de Figueiredo所组成并于1993年开发。 其设计目的是为了嵌入应用程序中，从而为应用程序提供灵活的扩展和定制功能。Lua由标准C编写而成，几乎在所有操作系统和平台上都可以编译，运行。Lua并没有提供强大的库，这是由它的定位决定的。所以Lua不适合作为开发独立应用程序的语言。Lua 有一个同时进行的JIT项目，提供在特定平台上的即时编译功能。

#####特性

#######轻量级

轻量级Lua语言的官方版本只包括一个精简的核心和最基本的库。这使得Lua体积小、启动速度快，从而适合嵌入在别的程序里。5.0.2版的Lua的内核小于120KB，而Python的内核大约860KB，Perl的内核大约1.1MB。

######可扩展

可扩展 Lua并不象其它许多"大而全"的语言那样，包括很多功能，比如网络通讯、图形界面等。但是Lua提供了非常易于使用的扩展接口和机制：由宿主语言(通常是C或C++)提供这些功能，Lua可以使用它们，就像是本来就内置的功能一样。

######其它特性

Lua还具有其它一些特性：同时支持面向过程编程和函数式编程（functional programming）；自动内存管理；只提供了一种通用类型的表（table），用它可以实现数组，哈希表，集合，对象；语言内置模式匹配；闭包(closure)；函数也可以看做一个值；提供多线程（协同进程 [5]，并非操作系统所支持的线程）支持；通过闭包和table可以很方便地支持面向对象编程所需要的一些关键机制，比如数据抽象，虚函数，继承和重载等。


####LuCI介绍

LuCI作为“FFLuCI”诞生于2008年3月份，目的是为OpenWrt固件从 Whiterussian 到 Kamikaze实现快速配置接口。Lua是一个小巧的脚本语言，很容易嵌入其它语言。轻量级 LUA语言的官方版本只包括一个精简的核心和最基本的库。这使得LUA体积小、启动速度快，从而适合嵌入在别的程序里。UCI是OpenWrt中为实现所有系统配置的一个统一接口，英文名Unified Configuration Interface，即统一配置接口。LuCI,即是这两个项目的合体，可以实现路由的网页配置界面。

最初开发这个项目的原因是没有一个应用于嵌入式的免费，干净，可扩展以及维护简单的网页用户界面接口。大部分相似的配置接口太依赖于大量的Shell脚本语言的应用，但是LuCi使用的是Lua编程语言，并将接口分为逻辑部分，如模板和视图。LuCI使用的是面向对象的库和模板，确保了高效的执行，轻量的安装体积，更快的执行速度以及最重要的一个特性————更好的可维护性。
与此同时，LuCI从MVC-Webframework衍生出一个包含了很多库、程序以及Lua程序用户接口的集合，但是LuCI仍然专注于实现网页用户界面并成为OpenWrt Kamikaze官方的一份子。
LuCI是一个开放源码的独立项目，欢迎任何人的加入。


#####模块Lua脚本和Luci快速配置接口的测试

#####Lua部分:

######1.lua解释器测试

在任意路径下输入

    vim hellolua.lua
    
新建一个Lua脚本文件`hellolua.lua`

写入以下代码：

    print("hello lua");

接着保存退出，在控制台上输入命令

    lua hellolua.lua
    
就能在控制台上看到

    hello lua
    
的输出，测试成功

######2.cgi解释lua文件测试

用`cd /www/cgi-bin`命令进入uhttpd的cgi-bin文件夹

接着使用`vim hellolua`新建一个名为“hellolua”的文件，注意没有后缀名

写入以下代码

    #!/usr/bin/lua
    print("hello lua")
    
之后在控制台用`chmod 777 hellolua`命令修改“hellolua”文件的权限为最高

接着用网页浏览器访问

    "192.168.1.1/cgi-bin/hellolua"
    
返回错误信息

Bad Gateway

The process did not produce any response

这是因为返回的消息没有http头消息导致了网页不能被浏览器正确的解析

修改代码如下：

    #!/usr/bin/lua
    print("Context-Type: test/plain\n")
    print("hello lua")
    
保存后刷新网页.就可以看到网页上显示：

    hello lua

测试成功。

######3.使用CGI环境变量

WEB 服务器和 CGI/FastCGI 程序之间交流信息的主要途径是环境变量 (以及标准输入输出流)

CGI环境变量列表：

***【table】***

环境变量 意义

SERVER_NAME CGI脚本运行时的主机名和IP地址.

SERVER_SOFTWARE 你的服务器的类型如： CERN/3.0 或 NCSA/1.3.

GATEWAY_INTERFACE 运行的CGI版本. 对于UNIX服务器, 这是CGI/1.1.

SERVER_PROTOCOL 服务器运行的HTTP协议. 这里当是HTTP/1.0.

SERVER_PORT 服务器运行的TCP口，通常Web服务器是80.

REQUEST_METHOD POST 或 GET, 取决于你的表单是怎样递交的.

HTTP_ACCEPT 浏览器能直接接收的Content-types, 可以有HTTP Accept header定义.

HTTP_USER_AGENT 递交表单的浏览器的名称、版本 和其他平台性的附加信息。

HTTP_REFERER 递交表单的文本的 URL，不是所有的浏览器都发出这个信息，不要依赖它

PATH_INFO 附加的路径信息, 由浏览器通过GET方法发出.

PATH_TRANSLATED 在PATH_INFO中系统规定的路径信息.

SCRIPT_NAME 指向这个CGI脚本的路径, 是在URL中显示的(如, /cgi-bin/thescript).

QUERY_STRING 脚本参数或者表单输入项(如果是用GET递交). QUERY_STRING 包含URL中问号后面的参数.

REMOTE_HOST 递交脚本的主机名，这个值不能被设置.

REMOTE_ADDR 递交脚本的主机IP地址.

REMOTE_USER 递交脚本的用户名. 如果服务器的authentication被激活，这个值可以设置。

REMOTE_IDENT 如果Web服务器是在ident (一种确认用户连接你的协议)运行, 递交表单的系统也在运行ident, 这个变量就含有ident返回值.

CONTENT_TYPE 如果表单是用POST递交, 这个值将是 application/x-www-form-urlencoded. 在上载文件的表单中, content-type 是个 multipart/form-data.

CONTENT_LENGTH 对于用POST递交的表单, 标准输入口的字节数.

通过查询Lua参考手册，得知Lua调用环境变量的函数为：

    os.getenv(string) --string为环境变量的名称
    
让我们来继续测试，仍然在`/www/cgi-bin`路径下，使用命令
    
    vim hellowlua
    
修改为

```
#!/usr/bin/lua
print("Context-Type: text/plain\n")  
print("your IP is "..os.getenv("REMOTE_ADDR"))
print("your querry is: "..os.getenv("QUERY_STRING"))
```

使用浏览器使用GET方法访问地址：

192.168.1.1/cgi-bin/hellolua?abc=123

页面将显示：

your IP is 192.168.1.102
your querry is: abc=123


如果需要调用模块上已经有的程序，可以通过以下Lua代码实现：

    os.execute(string) --string为要执行的语句
    
如果要调用模块上的设备，需要使用Lua的IO读写相关函数

例如在模块“ttyATH0” 串口上输出字符串“test1”的代码：

    io.output("/dev/ttyATH0");
    io.write("test1");

至此，模块Lua脚本的部分测试结束

####LuCI部分

#####Luci程序的位置

Luci的入口就在/www/cgi-bin/下的luci文件

但是它的主程序在/usr/lib/lua/luci中

#####如何添加LuCI应用

如果想添加自己的luci应用,需要在/usr/lib/lua/luci/controller下新建自己的应用

cd /usr/lib/lua/luci/controller

mkdir myapp

cd myapp

vim helloluci.lua
在打开的编辑器中输入以下代码

module("luci.controller.myapp.helloluci",package.seeall)
function index()

end
这就是一个luci应用的基本架构,

module函数后面要加上package.seeall参数是为了模块名以文件名命名，还有为了不污染全局变量_G，返回当前模块提供的函数等等。

在index()函数中要写明应用的访问路径

```
function index()
    entry({"myapp", "helloluci"}, call("helloluci_action"), "HelloLuci", 10).dependent=false
end
```

#####关于entry()函数的用法:

entry(path, target, title=nil, order=nil)
path 用一个表来描述应用的路径:例如{"myapp","helloluci"}将把应用路径设为 "192.168.1.1/cgi-bin/luci/myapp/helloluci"(官方解释是将你的结点(也就是应用)插入到myapp.helloluci中)

target 当用户访问这个路径的时候将执行的动作.有三种比较重要的动作"call","template","cbi"(后文会详述)

title 在菜单项中这个Luci应用的名字(可选项)
order 如果有两个应用有同样的order值,luci将会用一个菜单来显示他们(可选项)
在运行entry()的返回的表中可以修改一些属性

i18n 定义了当网页被请求时自动是否自动加载当地语言版本的网页版本(本地化相关)

dependent 当这个结点(luci应用)的父级无法找到时,保护这个结点不被调用

leaf 在这个节点开始就停止解析请求,并不再在调度树上前进。

sysauth 需要用户提供系统用户帐号来验证身份

#####关于entry函数中target参数

可以将call,template,cbi这三个动作分别依次对应MVC框架里面的 Controller View Model 三个部分

######call动作

call动作就是调用一个函数

例子

命令行下输入

cd /usr/lib/lua/luci/controller/myapp

vim helloluci.lua

将之前的代码换成如下代码:


```
module("luci.controller.myapp.mymodule", package.seeall)

function index()
    entry({"myapp", "helloluci"}, call("helloluci_action"), "HelloLuci", 10).dependent=false
end

function helloluci_action()
    luci.http.prepare_content("text/plain")
    luci.http.write("Hello Luci")
end
````

恩于是现在在浏览器中访问192.168.1.1/cgi-bin/luci/myapp/helloluci/

就能看到Hello Luci的字符

######template动作

个template的调用之直接调用一个htm文件,这个文件必须要在'/usr/lib/lua/luci/view'下

例子

在命令行输入

cd /usr/lib/lua/luci/view

mkdir myapp_view

cd myapp_view

vim helloworld.htm

在编辑器中输入一下代码:

```
<%+header%>
<h1><%:Hello World%></h1>


<%+footer%>
```

然后回到之前的helloluci.lua文件

命令行输入

cd /usr/lib/lua/luci/controller/myapp

vim helloluci.lua

把代码改为:

```
module("luci.controller.myapp.mylua",package.seeall)

function index()
        entry({"myapp","helloluci"},template("myapp_view/helloworld"),"HelloLuci",10).dependent=false
end
```

在template中作为存储网页格式信息的HTML文件，如果希望在其中插入自己的语句，只需要在需要插入语句的地方使用“<%”和”%>“标签对插入即可。

######cbi动作

依据官方介绍是可以快速的将/etc/config里面的配置文件用页面显示的工具

######注意事项

如果在修改应用的时候发现修改后再访问没有反应

出现如下的调试信息

No page is registered at '/sec/helloluci'.
If this url belongs to an extension, make sure it is properly installed.
If the extension was recently installed, try removing the /tmp/luci-indexcache file.

进入tmp/luci-modulecache执行rm *

删除所有cache文件,再次访问页面就可以正常显示。


